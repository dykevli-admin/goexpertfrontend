import React from 'react';
import { IRecievedRequest } from 'types/IProfileRequest';
import {
  Avatar, Button, Box,
} from '@mui/material';
import avatar from 'assets/images/profile-avatar.jpg';
import RECEIVED_CARD_STATUS from 'constants/receivedCardStatus';
import styles from './ReceivedCardMobile.module.scss';

interface ReceivedCardProps {
  receivedRequest: IRecievedRequest;
}

const RequestCardMobile: React.FC<ReceivedCardProps> = ({ receivedRequest }) => {
  const {
    client, date, price, status,
  } = receivedRequest;
  const { firstName, lastName } = client;
  const showPrice = `$${price}`;
  const requestDate = new Date(date);
  const [month, day, year, hour, minutes] = [
    requestDate.getMonth(), requestDate.getDate(), requestDate.getFullYear(),
    requestDate.getHours(), requestDate.getMinutes()];
  const showDate = `${hour}:${minutes} ${day}/${month}/${year}`;
  const buttonContent = RECEIVED_CARD_STATUS.filter((item) => status === item.name);

  return (
    <Box className={styles.card}>
      <Box className={styles.avatarBox}>
        <Avatar alt="avatar" src={avatar} />
        <div className={styles.name}>
          <span className={styles.name}>
            {firstName}
            {' '}
            {lastName}
          </span>
        </div>
      </Box>
      <Box className={styles.body}>
        <div className={styles.details}>
          <span className={styles.title}>Price</span>
          <span className={styles.info}>{showPrice}</span>
        </div>
        <div className={styles.details}>
          <span className={styles.title}>Request Time</span>
          <span className={styles.info}>{showDate}</span>
        </div>
        <div className={styles.details}>
          <span className={styles.title}>Status</span>
          <span className={styles.info}>
            <span className={styles.status}>{status}</span>
          </span>
        </div>
      </Box>
      {buttonContent.length > 0 && (
        <Box className={styles.statusBox}>
          <Button variant="contained" size="small" className={styles.acceptButton}>
            <span>{buttonContent[0].acceptButton}</span>
          </Button>
          <Button variant="contained" size="small" className={styles.rejectedButton}>
            <span>{buttonContent[0].rejectButton}</span>
          </Button>
        </Box>
      )}
    </Box>
  );
};

export default RequestCardMobile;
