import React from 'react';
import { IMyRequest } from 'types/IProfileRequest';
import {
  Avatar, Button, Box,
} from '@mui/material';
import avatar from 'assets/images/profile-avatar.jpg';
import REQUEST_CARD_STATUS from 'constants/requestCardStatus';
import styles from './RequestCardMobile.module.scss';

interface RequestCardProps {
  myRequest: IMyRequest;
}

const RequestCardMobile: React.FC<RequestCardProps> = ({ myRequest }) => {
  const {
    expert, date, price, status,
  } = myRequest;
  const { firstName, lastName, jobTitle } = expert;
  const showPrice = `$${price}`;
  const requestDate = new Date(date);
  const [month, day, year, hour, minutes] = [
    requestDate.getMonth(), requestDate.getDate(), requestDate.getFullYear(),
    requestDate.getHours(), requestDate.getMinutes()];
  const showDate = `${hour}:${minutes} ${day}/${month}/${year}`;
  const buttonContent = REQUEST_CARD_STATUS.filter((item) => status === item.name);

  return (
    <Box className={styles.card}>
      <Box className={styles.avatarBox}>
        <Avatar alt="avatar" src={avatar} />
        <div className={styles.name}>
          <span className={styles.name}>
            {firstName}
            {' '}
            {lastName}
          </span>
          <span className={styles.jobTitle}>{jobTitle}</span>
        </div>
      </Box>
      <Box className={styles.body}>
        <div className={styles.details}>
          <span className={styles.title}>Price</span>
          <span className={styles.info}>{showPrice}</span>
        </div>
        <div className={styles.details}>
          <span className={styles.title}>Request Time</span>
          <span className={styles.info}>{showDate}</span>
        </div>
        <div className={styles.details}>
          <span className={styles.title}>Status</span>
          <span className={styles.info}>
            <span className={styles.status}>{status}</span>
          </span>
        </div>
      </Box>
      {buttonContent.length > 0 && (
        <Box className={styles.statusBox}>
          <Button variant="contained" size="small" className={styles.button}>
            <span>{buttonContent[0].button}</span>
          </Button>
        </Box>
      )}
    </Box>
  );
};

export default RequestCardMobile;
