import React, { useState } from 'react';
import { PROFILE_SETTINGS_MENU } from 'constants/profileMenus';
import ArrowRightIcon from '@mui/icons-material/ArrowRight';
import styles from './ProfileTabs.module.scss';
import MyRequestsPage from '../MyRequestsPage';
import ReceivedRequestPage from '../ReceivedRequestPage';
import AccountSettingsPage from '../AccountSettingsPage';
import ProfileCard from '../ProfileCard';
import MobileRequestPage from '../MobileRequestPage';

interface IProfileTabProps {
  email: string;
  firstName: string;
}

const ProfileTabs: React.FC<IProfileTabProps> = (props) => {
  const { email, firstName } = props;
  const [active, setActive] = useState(PROFILE_SETTINGS_MENU[0]);
  const [isProfile, setIsProfile] = useState(true);
  const handleClick = (item) => {
    setIsProfile(false);
    setActive(item);
  };
  return (
    <>
      <div className={styles.desktopContainer}>
        <ProfileCard email={email} name={firstName} />
        <section className={styles.menu}>
          {PROFILE_SETTINGS_MENU.map((type) => (
            <button
              key={type}
              className={active === type ? styles.activetab : styles.tab}
              onClick={() => setActive(type)}
            >
              {type}
            </button>
          ))}
        </section>
        {active === PROFILE_SETTINGS_MENU[0] && <MyRequestsPage />}
        {active === PROFILE_SETTINGS_MENU[1] && <ReceivedRequestPage />}
        {active === PROFILE_SETTINGS_MENU[2] && <AccountSettingsPage />}
      </div>
      <>
        <div className={styles.mobileContainer}>
          <button className={styles.title} onClick={() => setIsProfile(true)}>
            <h2>{isProfile ? 'My Profile' : active}</h2>
          </button>
          {isProfile && (
            <>
              <ProfileCard email={email} name={firstName} />
              {PROFILE_SETTINGS_MENU.map((item) => (
                <div key={item}>
                  <button type="button" className={styles.menu} onClick={() => handleClick(item)}>
                    <span className={styles.text}>{item}</span>
                    <ArrowRightIcon className={styles.arrow} />
                  </button>
                </div>
              ))}
            </>
          )}
          {!isProfile && <MobileRequestPage active={active} />}
        </div>
      </>
    </>
  );
};

export default ProfileTabs;
