import React from 'react';
import { useMediaQuery } from '@mui/material';
import Header from 'layouts/Header';
import Breadcrumb from '../../components/Breadcrumb';
import Stepper from './Stepper';
import styles from './bookingPage.module.scss';

const BookingPage = () => {
  const isDesktop = useMediaQuery('(min-width: 900px)');

  return (
    <>
      <Header />
      <div className={styles.container}>
        {isDesktop ? <Breadcrumb /> : <h1 className={styles.title}>Booking</h1>}
        <Stepper />
      </div>
    </>
  );
};
export default BookingPage;
