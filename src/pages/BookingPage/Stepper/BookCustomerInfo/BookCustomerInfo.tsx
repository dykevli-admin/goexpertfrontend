import React, { useState } from 'react';
import {
  TextField, InputAdornment, IconButton, Button, Alert,
} from '@mui/material';
import EmailIcon from '@mui/icons-material/Email';
import { IBookInfo } from 'types/IMeeting';
import styles from './bookCustomerInfo.module.scss';

interface bookFormProps {
  handleNext: Function;
  handleChange: Function;
  bookDetail: IBookInfo;
  }

const BookCustomerInfo: React.FC<bookFormProps> = ({
  handleNext, handleChange, bookDetail,
}) => {
  const firstName = bookDetail?.firstName;
  const lastName = bookDetail?.lastName;
  const email = bookDetail?.email;

  const [errMsg, setErrMsg] = useState('');

  const handleValidation = () => {
    if (!firstName || !lastName || !email) {
      return setErrMsg('Please fill all information correctly!');
    }
    return handleNext();
  };

  return (
    <div className={styles.wrapper}>
      <h2 className={styles.heading}>Customer Information</h2>
      <TextField
        required
        fullWidth
        className={styles.input}
        label="First Name"
        name="firstName"
        value={firstName}
        onChange={handleChange('firstName')}
        variant="standard"
      />
      <TextField
        required
        fullWidth
        className={styles.input}
        label="Last Name"
        name="lastName"
        defaultValue={lastName}
        onChange={handleChange('lastName')}
        variant="standard"
      />
      <TextField
        required
        fullWidth
        className={styles.input}
        label="Email"
        name="email"
        value={email}
        onChange={handleChange('email')}
        variant="standard"
        InputProps={{
          endAdornment: (
            <InputAdornment position="end">
              <IconButton aria-label="email">
                <EmailIcon />
              </IconButton>
            </InputAdornment>
          ),
        }}
      />
      {errMsg ? <Alert severity="error">{errMsg}</Alert> : <></>}
      <Button onClick={handleValidation} className={styles.nextButton}>Next</Button>
    </div>
  );
};

export default BookCustomerInfo;
