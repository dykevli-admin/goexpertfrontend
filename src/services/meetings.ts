import request from 'utils/request';
import { AxiosRequestConfig } from 'axios';
import { IBookInfo } from '../types/IMeeting';

const generateMeeting = (params: IBookInfo) => {
  const config: AxiosRequestConfig = {
    url: '/meetings',
    method: 'POST',
    data: params,
  };

  return request(config);
};

export default generateMeeting;
